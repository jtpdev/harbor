package com.harbor.model;

import com.harbor.DynamicProperty;
import com.harbor.annotations.Box;
import com.harbor.annotations.Title;
import com.vaadin.data.util.ObjectProperty;

public class ModelTest extends DynamicProperty {
	
	@Title(title = "Model Test")
	@Box
	private ObjectProperty<EModelTest> eModelTest;

	@Override
	public Object toModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void toProperty(Object model) {
		// TODO Auto-generated method stub

	}

	public ObjectProperty<EModelTest> geteModelTest() {
		return eModelTest;
	}

	public void seteModelTest(ObjectProperty<EModelTest> eModelTest) {
		this.eModelTest = eModelTest;
	}

}
