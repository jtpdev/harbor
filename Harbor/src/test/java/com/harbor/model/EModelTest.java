package com.harbor.model;

public enum EModelTest {
	
	SIM("Sim"),
	NAO("Não");
	
	private String descricao;

	private EModelTest(String descricao) {
		this.setDescricao(descricao);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
