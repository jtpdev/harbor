package com.harbor.enums;

public enum EIconButton {

	NONE("/caminho/"),
	SUCCESS("/caminho/"),
	ALERT("/caminho/"),
	ERROR("/caminho/");
	
	private String path;
	
	private EIconButton(String path) {
		this.path = path;
	}
	
	@Override
	public String toString() {
		return path;
	}
	
}
