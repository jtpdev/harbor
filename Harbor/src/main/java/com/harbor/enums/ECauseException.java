package com.harbor.enums;

import java.util.Locale;

import com.harbor.exceptions.HarborException;

public enum ECauseException {
	
	TITLE_ANNOTATION_NOT_PRESENT(1L, "O campo %s da classe %s deve ser anotado com Title.", "O mesmo em inglês"),
	FIELD_NOT_A_OBJECT_PROPERTY(2L, "O campo %s da classe %s deve ser do tipo ObjectProperty.", "O mesmo em inglês"),
	VALUE_FIELD_NOT_A_ENUM(3L, "O valor do campo %s da classe %s anotado com Box, deve ser ou extender uma Enum.", "O mesmo em inglês"),
	;
	
	private Long code;
	private String causePortuguese;
	private String causeEnglish;
	
	private ECauseException(Long code, String causePortuguese, String causeEnglish) {
		this.code = code;
		this.causePortuguese = causePortuguese;
		this.causeEnglish = causeEnglish;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public String getCausePortuguese() {
		return causePortuguese;
	}

	public void setCausePortuguese(String causePortuguese) {
		this.causePortuguese = causePortuguese;
	}

	public String getCauseEnglish() {
		return causeEnglish;
	}

	public void setCauseEnglish(String causeEnglish) {
		this.causeEnglish = causeEnglish;
	}
	
	@Override
	public String toString() {
		if(Locale.getDefault().getLanguage().contains("pt")){
			return getCausePortuguese();
		}
		return getCauseEnglish();
	}
	
	public static void main(String[] args) throws Exception {
//		System.out.println(String.format(ECauseException.TITLE_ANNOTATION_NOT_PRESENT.toString(), ECauseException.class.getField("TITLE_ANNOTATION_NOT_PRESENT").getName(), ECauseException.class.getName()));
		throw new HarborException(ECauseException.TITLE_ANNOTATION_NOT_PRESENT,  ECauseException.class.getField("TITLE_ANNOTATION_NOT_PRESENT").getName(), ECauseException.class.getName());
	}

}
