package com.harbor;

import java.lang.reflect.Field;

import com.harbor.annotations.Title;
import com.harbor.enums.ECauseException;
import com.harbor.exceptions.HarborException;
import com.vaadin.data.util.ObjectProperty;

public class Validate {
	

	public static <P extends DynamicProperty> void verifyObjectProperty(Class<P> clazz, Field field, Object object) {
		if(!ObjectProperty.class.isInstance(object)){
			throw new HarborException(ECauseException.FIELD_NOT_A_OBJECT_PROPERTY, field.getName(), clazz.getName());
		};
	}

	public static <P extends DynamicProperty> void verifyEnumClass(Class<P> clazz, Field field, Object value) {
		if(!Enum.class.isInstance(value)){
			throw new HarborException(ECauseException.VALUE_FIELD_NOT_A_ENUM, field.getName(), clazz.getName());
		}
	}

	public static <P extends DynamicProperty> void verifyTitleAnntotation(Class<P> clazz, Field field) {
		if(!field.isAnnotationPresent(Title.class)){
			throw new HarborException(ECauseException.TITLE_ANNOTATION_NOT_PRESENT, field.getName(), clazz.getName());
		}
	}

}
