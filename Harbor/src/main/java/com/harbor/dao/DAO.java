package com.harbor.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.Criteria;
import org.hibernate.Session;

/**
 * This class is the DAO class main
 * 
 * @author Jimmy Porto
 *
 */
public class DAO {
	
	private EntityManagerFactory factory;
    private EntityManager manager;
    
    public DAO() {
		 factory = Persistence.createEntityManagerFactory("ECommerce");
		 manager = factory.createEntityManager();
	}
    
    public void close() {
    	manager.close();
        factory.close();
    }

	public void delete(Object toDel) {
		manager.getTransaction().begin();
 		Object del = manager.merge(toDel);
		manager.remove(del);
		manager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public <T> T find(Object id, Class<T> clazz) {
		if(id == null)
			return null;
		EntityTransaction transaction = manager.getTransaction();
		if (!transaction.isActive()) {
			transaction.begin();
		} 
		Object find = manager.find(clazz, id);
		if (transaction.isActive()) {
			transaction.commit();
		} 
		return (T) find;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> listOfClass(Class<T> clazz) {
		Session sessao = (Session) manager.getDelegate();
        Criteria crit = sessao.createCriteria(clazz);
        return crit.list();
	}

	public <T> T save(T toSave) {
			manager.getTransaction().begin();
			T merge = manager.merge(toSave);
			manager.getTransaction().commit();
			return merge;
	}

}
