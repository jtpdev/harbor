package com.harbor;

import java.lang.reflect.Field;

import com.harbor.annotations.Box;
import com.harbor.annotations.Check;
import com.harbor.annotations.DateView;
import com.harbor.annotations.Decimal;
import com.harbor.annotations.Numeric;
import com.harbor.annotations.Table;
import com.harbor.annotations.TableCol;
import com.harbor.annotations.TableLoad;
import com.harbor.annotations.Text;
import com.harbor.annotations.Title;
import com.harbor.dao.DAO;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.ComboBox;

/**
 * This class make all stuff.
 * 
 * @author Jimmy Porto
 *
 */
public class StuffMaker {
	

	public static <P extends DynamicProperty> void make(DAO dao, Class<P>... clazzs) throws Exception{
		for (Class<P> clazz : clazzs) {
			HarborUI ui = new HarborUI(dao);
			P property = clazz.newInstance();
			makeAll(clazz, property, ui);
		}
	}

	public static <P extends DynamicProperty> void make(Class<P>... clazzs) throws Exception{
		for (Class<P> clazz : clazzs) {
			P instance = clazz.newInstance();
			HarborUI ui = new HarborUI();
			makeAll(clazz, instance, ui);
		}
			
	}

	
	/** TODO Verificar no Arquivo Annotations.docx como será feito e gerenciado as annotations {@link Table}, {@link TableLoad}, {@link TableCol}
	 * 
	 */
	private static <P extends DynamicProperty> void makeAll(Class<P> clazz, P instance, HarborUI ui) throws Exception {
		PropertysetItem item = new PropertysetItem();
		FieldGroup binder = new FieldGroup(item);
		for (Field field : clazz.getFields()) {
			field.setAccessible(true);
			Validate.verifyTitleAnntotation(clazz, field);
			Object object = field.get(instance);
			Validate.verifyObjectProperty(clazz, field, object);
			Object value = ((ObjectProperty) object).getValue();
			if(field.isAnnotationPresent(Box.class)){
				float size = field.getAnnotation(Box.class).viewSize();
				Validate.verifyEnumClass(clazz, field, value);
				ComboBox cb = new ComboBox(field.getAnnotation(Title.class).title());
				cb.setWidth(size, Unit.PIXELS);
				ui.getLayout().addComponent(cb);
				createBinging(item, binder, field, object, cb);
			}
			if(field.isAnnotationPresent(Check.class)){
				
			}
			if(field.isAnnotationPresent(DateView.class)){
				
			}
			if(field.isAnnotationPresent(Decimal.class)){
				
			}
			if(field.isAnnotationPresent(Numeric.class)){
				
			}
			if(field.isAnnotationPresent(Text.class)){
				
			}
		}
	}

	private static void createBinging(PropertysetItem item, FieldGroup binder, Field field, Object object, com.vaadin.ui.Field<?> comp) {
		item.addItemProperty(field.getName(), (ObjectProperty) object);
		binder.bind(comp, field.getName());
	}
}
