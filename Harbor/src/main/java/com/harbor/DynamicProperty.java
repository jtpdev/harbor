package com.harbor;

/**
 * This class is responsible for transform a model in a property and vice versa.
 * It also have behaviors.
 * 
 * @author Jimmy Tinelli Porto
 *
 * @param <M>
 */
public abstract class DynamicProperty<M> {

	/**
	 * Transform a property in a model
	 * 
	 * @return
	 */
	public abstract M toModel();

	/**
	 * Transform a model in a property
	 * 
	 * @param model
	 */
	public abstract void toProperty(M model);

	/**
	 * The view to call.
	 */
	@SuppressWarnings("rawtypes")
	private DynamicProperty view;

	/**
	 * Get the view to call.
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public DynamicProperty getViewToCall() {
		return view;
	}

	/**
	 * Set the view to call. Used to indicate to current view, where to return,
	 * if necessary. Not necessarily being the view that called this view. And
	 * may be a new view. Used to show the way to follow it.
	 * 
	 * It should be used when you need search and wish open a new view and
	 * returns to the same. Or you need returns in other view already opened or
	 * open a new view too.
	 * 
	 * @param previous
	 */
	@SuppressWarnings("rawtypes")
	public void setViewToCall(DynamicProperty view) {
		this.view = view;
	}

}
