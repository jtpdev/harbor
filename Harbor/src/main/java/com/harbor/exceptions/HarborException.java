package com.harbor.exceptions;

import com.harbor.enums.ECauseException;

public class HarborException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	private ECauseException eHarborCause;

	public HarborException(ECauseException eHarborCause, Object... args) {
		super();
		this.seteHarborCause(eHarborCause);
		this.setMessage(String.format(getHarborCause(), args));
	}

	public String getHarborCause() {
		return eHarborCause.toString();
	}
	
	public Long getHarborCauseCode() {
		return eHarborCause.getCode();
	}

	public ECauseException geteHarborCause() {
		return eHarborCause;
	}

	public void seteHarborCause(ECauseException eHarborCause) {
		this.eHarborCause = eHarborCause;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
