package com.harbor;

import com.harbor.dao.DAO;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

public class HarborUI extends UI {
	
	private DAO dao;
	private HarborLayout layout;
	
	public HarborUI() {
		this(new DAO());
	}
	
	public HarborUI(DAO dao){
		this.setDao(dao);
		setLayout(new HarborLayout());
		setContent(getLayout());
	}

	@Override
	protected void init(VaadinRequest request) {
		
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	public HarborLayout getLayout() {
		return layout;
	}

	public void setLayout(HarborLayout layout) {
		this.layout = layout;
	}

}
