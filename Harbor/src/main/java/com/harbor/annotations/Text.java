package com.harbor.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Text {
	
	int minSize() default -1;
	int maxSize() default -1;
	int viewSize() default 100;
	int size() default -1;
	String mask() default "";

}
