package com.harbor.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Line {

	int value();
	int sequence() default 0;
	int colSpan() default 1;
	int rowSpan() default 1;
	
}
