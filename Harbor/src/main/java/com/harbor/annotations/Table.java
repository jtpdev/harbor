package com.harbor.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javafx.scene.layout.Region;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {

	// TODO mudar aqui para região default do Vaadin
	double widthSize() default Region.USE_COMPUTED_SIZE;
	double heigthSize() default Region.USE_COMPUTED_SIZE;

	Class<?> properties();
	
}
